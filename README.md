# UniversitySelectionStrategies

#### 介绍
这是一个大学可以分类查找内地高校的应用

#### 软件架构
1. rawfile提供信息数据。
2. 点击首页按钮，直达对应类别名录。
3. 利用HarmonOS特有的分布式能力，可以流转详情页。


#### 安装教程

1.  下载：DevEco Studio
2.  打开：在DevEco Studio中打开文件
3.  运行：模拟运行

#### 使用说明

1.  点击首页选项，可以进入对应类别的高校名录。
2.  点击高校名录，可以进入对应详情页。
3.  点击分享，可以把该详情页，流转到别的设备。


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
