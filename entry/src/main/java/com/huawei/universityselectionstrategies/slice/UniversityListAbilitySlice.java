package com.huawei.universityselectionstrategies.slice;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huawei.universityselectionstrategies.MainAbility;
import com.huawei.universityselectionstrategies.ResourceTable;
import com.huawei.universityselectionstrategies.bean.UniversityInfo;
import com.huawei.universityselectionstrategies.provider.UniversityListProvider;
import com.huawei.universityselectionstrategies.utils.CommonUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.List;


public class UniversityListAbilitySlice extends AbilitySlice {

    private ListContainer universityListContainer;
    private List<UniversityInfo> totalUniversityDataList;
    private List<UniversityInfo> universityDataList;
    private UniversityListProvider universityListProvider;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_university_list_layout);
        initData();
        universityListContainer = findComponentById(ResourceTable.Id_university_container);
        universityListContainer.setItemProvider(universityListProvider);
        universityListProvider.notifyDataChanged();
        String intentType = intent.getStringParam("key");
        Text listTag = findComponentById(ResourceTable.Id_list_head_tag);
        listTag.setText("类别：" + intentType);
        universityDataList.clear();
        for (UniversityInfo tempListData : totalUniversityDataList) {
            if (intentType.equals("all")) {
                universityDataList.add(tempListData);
            }else {
                if (intentType.equals("985") || intentType.equals("211") || intentType.equals("双一流")) {
                    if (intentType.equals(tempListData.getType())) {
                        universityDataList.add(tempListData);
                    }
                } else {
                    if (intentType.equals(tempListData.getName())) {
                        listTag.setText("搜索结果：");
                        universityDataList.add(tempListData);
                    }
                }
            }
        }

        updateListView();
        initListener();

    }

    private void initData() {
        Gson gson = new Gson();
        totalUniversityDataList =
                gson.fromJson(
                        CommonUtils.getStringFromJsonPath(this,"entry/resources/rawfile/university_datas.json"),
                        new TypeToken<List<UniversityInfo>>() { }.getType());
        universityDataList = new ArrayList<>();
        universityDataList.addAll(totalUniversityDataList);
        universityListProvider = new UniversityListProvider(universityDataList,this);
    }

    private void initListener() {

        universityListContainer.setItemClickedListener(
                (listContainer, component, i, l) -> {
                    Intent intent = new Intent();
                    Operation operation =
                            new Intent.OperationBuilder()
                                    .withBundleName(getBundleName())
                                    .withAbilityName(MainAbility.class.getName())
                                    .withAction("action.detail")
                                    .build();
                    intent.setOperation(operation);
                    intent.setParam(UniversityDetailAbilitySlice.INTENT_NAME,universityDataList.get(i).getName());
                    intent.setParam(UniversityDetailAbilitySlice.INTENT_TYPE,universityDataList.get(i).getType());
                    intent.setParam(UniversityDetailAbilitySlice.INTENT_CONTENT,universityDataList.get(i).getContent());
                    intent.setParam(UniversityDetailAbilitySlice.INTENT_IMAGE,universityDataList.get(i).getImgUrl());
                    intent.setParam(UniversityDetailAbilitySlice.INTENT_URI,universityDataList.get(i).getUri());
                    startAbility(intent);
                });

    }

    private void updateListView() {
        universityListProvider.notifyDataChanged();
        universityListContainer.invalidate();
        universityListContainer.scrollToCenter(0);
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
