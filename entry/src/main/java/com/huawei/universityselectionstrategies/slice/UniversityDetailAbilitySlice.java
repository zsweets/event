package com.huawei.universityselectionstrategies.slice;

import com.huawei.universityselectionstrategies.MainAbility;
import com.huawei.universityselectionstrategies.ResourceTable;
import com.huawei.universityselectionstrategies.distribute.api.SelectDeviceResultListener;
import com.huawei.universityselectionstrategies.distribute.factory.DeviceSelector;
import com.huawei.universityselectionstrategies.utils.CommonUtils;
import com.huawei.universityselectionstrategies.utils.LogUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.continuation.ExtraParams;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.utils.net.Uri;

public class UniversityDetailAbilitySlice extends AbilitySlice {
    public static final String INTENT_NAME = "intent_name";
    public static final String INTENT_TYPE = "intent_type";
    public static final String INTENT_CONTENT = "intent_content";
    public static final String INTENT_IMAGE = "intent_image";
    public static final String INTENT_URI = "intent_uri";
    public static final String INTENT_COMMENT = "intent_comment";
    private DependentLayout parentLayout;
    private TextField commentFocus;
    private Image iconShared;
    private Image iconUri;
    private DeviceSelector deviceSelector;
    private String name;
    private String type;
    private String content;
    private String image;
    private String uri;
    private String comment;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_university_detail_layout);
        name = intent.getStringParam(INTENT_NAME);
        type = intent.getStringParam(INTENT_TYPE);
        content = intent.getStringParam(INTENT_CONTENT);
        image = intent.getStringParam(INTENT_IMAGE);
        uri = intent.getStringParam(INTENT_URI);
        comment = intent.getStringParam(INTENT_COMMENT);
        initView();
        initListener();
        initDistributeComponent();
    }

    private void initView() {
        parentLayout = findComponentById(ResourceTable.Id_parent_layout);
        commentFocus = findComponentById(ResourceTable.Id_text_file);
        iconShared = findComponentById(ResourceTable.Id_button4);
        iconUri = findComponentById(ResourceTable.Id_button3);
        Text detailType = findComponentById(ResourceTable.Id_scroll_view_title);
        Text detailTitle = findComponentById(ResourceTable.Id_title_text);
        Text detailContent = findComponentById(ResourceTable.Id_second_title);
        Image detailImage = findComponentById(ResourceTable.Id_image_content);
        detailType.setText("类别: " + type);
        detailTitle.setText("名称: " + name);
        detailContent.setText(content);
        commentFocus.setText(comment);
        detailImage.setPixelMap(CommonUtils.getPixelMapFromPath(this, image));
    }


    private void initListener() {
        parentLayout.setTouchEventListener(
                //监听触控焦点是否在按钮上
                (component, touchEvent) -> {
                    if (commentFocus.hasFocus()) {
                        commentFocus.clearFocus();
                    }
                    return true;
                });
        iconShared.setClickedListener(component -> deviceSelector.showDistributeDevices(
                new String[]{ExtraParams.DEVICETYPE_SMART_PAD, ExtraParams.DEVICETYPE_SMART_PHONE},
                null));
        iconUri.setClickedListener(component -> {
            Intent intentUri = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withUri(Uri.parse(uri))
                    .build();
            intentUri.setOperation(operation);
            startAbility(intentUri);
        });
    }

    private void initDistributeComponent() {
        deviceSelector = new DeviceSelector();
        deviceSelector.setup(getAbility());
        deviceSelector.setSelectDeviceResultListener(new SelectDeviceResultListener() {
            //通过startAbility方法拉起了指定的FA，并将intent携带的参数一并传递过去
            @Override
            public void onSuccess(DeviceInfo info) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId(info.getDeviceId())
                        .withBundleName(getBundleName())
                        .withAbilityName(MainAbility.class.getName())
                        .withAction("action.detail")
                        .withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE)
                        .build();
                intent.setOperation(operation);
                intent.setParam(UniversityDetailAbilitySlice.INTENT_NAME, name);
                intent.setParam(UniversityDetailAbilitySlice.INTENT_TYPE, type);
                intent.setParam(UniversityDetailAbilitySlice.INTENT_CONTENT, content);
                intent.setParam(UniversityDetailAbilitySlice.INTENT_URI, uri);
                intent.setParam(UniversityDetailAbilitySlice.INTENT_IMAGE, image);
                intent.setParam(UniversityDetailAbilitySlice.INTENT_COMMENT,commentFocus.getText());
                startAbility(intent);
            }

            @Override
            public void onFail(DeviceInfo info) {
                LogUtils.error("cwq","onFail is called,info is "+info.getDeviceState());
            }
        });
    }
}