package com.huawei.universityselectionstrategies.slice;

import com.huawei.universityselectionstrategies.MainAbility;
import com.huawei.universityselectionstrategies.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.components.element.FrameAnimationElement;

import static com.huawei.universityselectionstrategies.MainAbility.myIntent;

public class MainAbilitySlice extends AbilitySlice {
    private Button btnAll;
    private Button btn985;
    private Button btnDf;
    private Button btn211;
    private Button btnSearch;
    public TextField myTextField;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_main_layout);

        FrameAnimationElement frameAnimationElement = new FrameAnimationElement(getContext(),ResourceTable.Graphic_animal_element);
        Component image = findComponentById(ResourceTable.Id_image_animation);
        image.setBackground(frameAnimationElement);

        btnAll = findComponentById(ResourceTable.Id_main_all_btn);
        btn985 = findComponentById(ResourceTable.Id_main_985_btn);
        btnDf = findComponentById(ResourceTable.Id_main_df_btn);
        btn211 = findComponentById(ResourceTable.Id_main_211_btn);
        btnSearch = findComponentById(ResourceTable.Id_btn_search);
        myTextField = findComponentById(ResourceTable.Id_textField);

        initBtnCLick();
        frameAnimationElement.start();
    }

    private void initBtnCLick() {

        btnAll.setClickedListener(Component -> {
            myIntent.setParam("key","all");
            present(MainAbility.myUniversityListAbilitySlice,myIntent);
        });

        btn985.setClickedListener(component -> {
            myIntent.setParam("key","985");
            present(MainAbility.myUniversityListAbilitySlice,myIntent);
        });

        btnDf.setClickedListener(component -> {
            myIntent.setParam("key","双一流");
            present(MainAbility.myUniversityListAbilitySlice,myIntent);
        });

        btn211.setClickedListener(component -> {
            myIntent.setParam("key","211");
            present(MainAbility.myUniversityListAbilitySlice,myIntent);
        });

        btnSearch.setClickedListener(component -> {
            myIntent.setParam("key",myTextField.getText());
            present(MainAbility.myUniversityListAbilitySlice,myIntent);
        });
    }
}
