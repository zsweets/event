package com.huawei.universityselectionstrategies.provider;

import com.huawei.universityselectionstrategies.ResourceTable;
import com.huawei.universityselectionstrategies.bean.UniversityInfo;
import com.huawei.universityselectionstrategies.utils.CommonUtils;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class UniversityListProvider extends BaseItemProvider {
    private final List<UniversityInfo> universityInfoList;
    private final Context context;

    public UniversityListProvider(List<UniversityInfo> listBasicInfo, Context context) {
        this.universityInfoList = listBasicInfo;
        this.context = context;
    }

    @Override
    public int getCount() {
        return universityInfoList == null ? 0 : universityInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return universityInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        Component temp = component;
        if (temp == null) {
            temp = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_university_item_layout, null, false);
            viewHolder = new ViewHolder();
            viewHolder.name = temp.findComponentById(ResourceTable.Id_item_university_name);
            viewHolder.image = temp.findComponentById(ResourceTable.Id_item_university_image);
            temp.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) temp.getTag();
        }
        viewHolder.name.setText(universityInfoList.get(i).getName());
        viewHolder.image.setPixelMap(CommonUtils.getPixelMapFromPath(context, universityInfoList.get(i).getImgUrl()));
        return temp;
    }

    private static class ViewHolder {
        Text name;
        Image image;
    }
}
