package com.huawei.universityselectionstrategies.bean;

public class UniversityInfo {
    private final String name;

    private final String type;

    private final String imgUrl;

    private final String content;

    private final String uri;

    public UniversityInfo(String name, String type, String imgUrl, String content, String uri) {
        this.name = name;
        this.type = type;
        this.imgUrl = imgUrl;
        this.content = content;
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getContent() {
        return content;
    }

    public String getUri() {
        return uri;
    }

}
