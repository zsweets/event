package com.huawei.universityselectionstrategies;

import com.huawei.universityselectionstrategies.slice.MainAbilitySlice;
import com.huawei.universityselectionstrategies.slice.UniversityDetailAbilitySlice;
import com.huawei.universityselectionstrategies.slice.UniversityListAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.IBundleManager;

public class MainAbility extends Ability {
    private static final String PERMISSION_DATASYNC = "ohos.permission.DISTRIBUTED_DATASYNC";
    private static final int MY_PERMISSION_REQUEST_CODE = 1;

    public static UniversityListAbilitySlice myUniversityListAbilitySlice = new UniversityListAbilitySlice();
    public static Intent myIntent = new Intent();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        addActionRoute("action.detail", UniversityDetailAbilitySlice.class.getName());

        if (verifySelfPermission(PERMISSION_DATASYNC) != IBundleManager.PERMISSION_GRANTED) {
            if (canRequestPermission(PERMISSION_DATASYNC)) {
                requestPermissionsFromUser(new String[] {PERMISSION_DATASYNC}, MY_PERMISSION_REQUEST_CODE);
            }
        }
    }
}
